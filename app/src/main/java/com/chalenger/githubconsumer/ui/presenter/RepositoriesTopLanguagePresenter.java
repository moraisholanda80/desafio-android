package com.chalenger.githubconsumer.ui.presenter;


import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;

import java.util.List;

/**
 * Created by sergio on 30/06/16.
 */
public interface RepositoriesTopLanguagePresenter {

   void onLoad();

    void onLoadMore();

   List<LanguageGithub> repositoriesParcelable();

    void restoreParcelable(List<LanguageGithub> languageGithubList);
}
