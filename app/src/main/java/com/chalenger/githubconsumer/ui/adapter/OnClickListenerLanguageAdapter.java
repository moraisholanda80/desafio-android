package com.chalenger.githubconsumer.ui.adapter;


import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;

/**
 * Created by sergio on 30/06/16.
 */
public interface OnClickListenerLanguageAdapter {

    void setItemClick(LanguageGithub languageGithub);
}
