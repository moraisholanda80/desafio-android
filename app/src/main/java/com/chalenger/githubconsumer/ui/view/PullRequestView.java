package com.chalenger.githubconsumer.ui.view;


import android.content.Intent;

import com.chalenger.githubconsumer.domain.data.dto.PullRequest;

import java.util.List;

/**
 * Created by sergio on 21/06/16.
 */
public interface PullRequestView {

    void showPullRequest(List<PullRequest> pullRequestList);

    void showError(String message);

    void hideLoading();

    void showLoading();

    void showRetry();

    void hideRetry();

    void launchActivity(Intent intent);

    boolean isFragmentActive();
}
