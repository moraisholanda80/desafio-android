package com.chalenger.githubconsumer.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chalenger.githubconsumer.R;
import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.ui.viewholder.AbstractRecyclerViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by sergio on 30/06/16.
 */
public class RepositoriesTopLanguageAdapter extends RecyclerView.Adapter<AbstractRecyclerViewHolder> {


    private OnClickListenerLanguageAdapter listenerLanguageAdapter;
    private List<LanguageGithub> languageGithubList = new ArrayList<>();

    public void setListenerLanguageAdapter(OnClickListenerLanguageAdapter listenerLanguageAdapter) {
        this.listenerLanguageAdapter = listenerLanguageAdapter;
    }

    public void add(List<LanguageGithub> languageGithubList){
        this.languageGithubList.addAll(languageGithubList);
        notifyDataSetChanged();

    }
    public void addMoore(LanguageGithub languageGithub){
        this.languageGithubList.add(this.languageGithubList.size(),languageGithub) ;
        notifyItemChanged(this.languageGithubList.size());
    }
    @Override
    public AbstractRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_item,parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AbstractRecyclerViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        LanguageGithub languageGithub = languageGithubList.get(position);
        itemViewHolder.textView.setText(languageGithub.name);
        itemViewHolder.textViewDescription.setText(languageGithub.description);
        itemViewHolder.textViewAuthor.setText(languageGithub.getNameOwner());
        itemViewHolder.textViewCountForks.setText(String.valueOf(languageGithub.countForks));
        itemViewHolder.textViewCountStar.setText(String.valueOf(languageGithub.stars));
        Glide.with(itemViewHolder.view.getContext())
                .load(languageGithub.getUrlAvatar())
                .bitmapTransform(new CropCircleTransformation(itemViewHolder.view.getContext()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(itemViewHolder.imageView);
        itemViewHolder.itemRow.setOnClickListener(v -> {
            if(listenerLanguageAdapter !=null){
                listenerLanguageAdapter.setItemClick(languageGithub);
            }
        });

    }


    @Override
    public int getItemCount() {
        return languageGithubList.size();
    }


    class ItemViewHolder extends AbstractRecyclerViewHolder {
        View view;
        @BindView(R.id.author_avatar)
        ImageView imageView;
        @BindView(R.id.name_repository)
        TextView textView;
        @BindView(R.id.item_row)
        RelativeLayout itemRow;
        @BindView(R.id.description_repository)
        TextView textViewDescription;
        @BindView(R.id.author)
        TextView textViewAuthor;
        @BindView(R.id.fork_count)
        TextView textViewCountForks;
        @BindView(R.id.star_count)
        TextView textViewCountStar;
        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;

        }
    }


}
