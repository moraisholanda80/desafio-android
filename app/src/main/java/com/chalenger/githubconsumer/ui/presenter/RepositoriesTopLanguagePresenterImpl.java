package com.chalenger.githubconsumer.ui.presenter;


import com.chalenger.githubconsumer.GithubConsumerApplication;
import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.error.DefaultErrorBundle;
import com.chalenger.githubconsumer.domain.data.error.ErrorBundle;
import com.chalenger.githubconsumer.domain.data.error.ErrorMessageFactory;
import com.chalenger.githubconsumer.domain.data.error.ResultNotFoundException;
import com.chalenger.githubconsumer.domain.data.error.NetworkConnectionException;
import com.chalenger.githubconsumer.domain.data.response.SimpleObserver;
import com.chalenger.githubconsumer.domain.repository.ILanguagesGithubRepository;
import com.chalenger.githubconsumer.domain.repository.LanguagesGithubRepository;
import com.chalenger.githubconsumer.ui.view.RepositoriesTopLanguageListView;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sergio on 309/06/16.
 */
public class RepositoriesTopLanguagePresenterImpl implements RepositoriesTopLanguagePresenter {


    private ILanguagesGithubRepository repository;
    private RepositoriesTopLanguageListView repositoriesTopLanguageListView;
    private List<LanguageGithub> languageGithubList;
    private int page = 0;


    public RepositoriesTopLanguagePresenterImpl(RepositoriesTopLanguageListView repositoriesTopLanguageListView) {
        this.repositoriesTopLanguageListView = repositoriesTopLanguageListView;
        this.repository = new LanguagesGithubRepository();
        this.languageGithubList = new ArrayList<>();
    }

    @Override
    public void onLoad() {
        page=1;
        validateConnectivity();
        loadRemote();
    }

    private void loadRemote(){
        if(page==1)
        repositoriesTopLanguageListView.showLoading();
            repository.loadRespositories("Java","stars",page)
                    .subscribe(new SimpleObserver<List<LanguageGithub>>(){
                        @Override
                        public void onNext(List<LanguageGithub> list) {
                            languageGithubList = list;
                            if(languageGithubList.size()==0){
                                shorErrorMessage(new DefaultErrorBundle(new ResultNotFoundException()));
                            }else {
                                repositoriesTopLanguageListView.hideLoading();
                                repositoriesTopLanguageListView.showLanguages(list);
                            }
                        }
                        @Override
                        public void onError(Throwable e) {
                            repositoriesTopLanguageListView.hideLoading();
                            shorErrorMessage(new DefaultErrorBundle((Exception) e));
                        }
                        @Override
                        public void onCompleted() {
                            repositoriesTopLanguageListView.hideLoading();
                            repositoriesTopLanguageListView.hideRetry();
                        }
                    });
    }

    private void validateConnectivity(){
        new ReactiveNetwork().observeInternetConnectivity()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::notifiyConnectivity);

    }
    private void notifiyConnectivity(boolean isConnectedToInternet){
        if(!isConnectedToInternet){
            repositoriesTopLanguageListView.hideLoading();
            repositoriesTopLanguageListView.showRetry();
            shorErrorMessage(new DefaultErrorBundle(new NetworkConnectionException()));
        }else{
            loadRemote();
        }
    }
    @Override
    public void onLoadMore() {
        page++;
        loadRemote();
    }

    private void shorErrorMessage(ErrorBundle errorBundle){
        repositoriesTopLanguageListView.showError(ErrorMessageFactory.create(GithubConsumerApplication.getInstance(),errorBundle.getException()));
    }

    @Override
    public List<LanguageGithub> repositoriesParcelable() {
        return (List<LanguageGithub>) ((ArrayList<LanguageGithub>) languageGithubList).clone();
    }

    @Override
    public void restoreParcelable(List<LanguageGithub> languageGithubList) {
        this.languageGithubList = languageGithubList;
        repositoriesTopLanguageListView.showLanguages(languageGithubList);
    }
}
