package com.chalenger.githubconsumer.ui.adapter;


import com.chalenger.githubconsumer.domain.data.dto.PullRequest;

/**
 * Created by sergio on 30/06/16.
 */
public interface OnClickListenerPullRequestAdapter {

    void setItemClick(PullRequest pullRequest);
}
