package com.chalenger.githubconsumer.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.chalenger.githubconsumer.R;
import com.chalenger.githubconsumer.ui.fragments.RepositoriesTopGithubFragment;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by sergio on 19/06/16.
 */
public class RepositoriesTopLanguageListActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        initToolbar();
        if (savedInstanceState == null) {
            addFragment(R.id.fragmentContainer, new RepositoriesTopGithubFragment());
        }
    }
    private void initToolbar() {
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }
}
