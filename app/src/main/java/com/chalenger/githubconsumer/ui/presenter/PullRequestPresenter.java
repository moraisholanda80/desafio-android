package com.chalenger.githubconsumer.ui.presenter;


import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.dto.PullRequest;

/**
 * Created by sergio on 30/06/16.
 */
public interface PullRequestPresenter {

    void loadPullRequest(LanguageGithub languageGithub);
    void onLoad();
    void onClickPullRequest(PullRequest pullRequest);
}
