package com.chalenger.githubconsumer.ui.activities;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.Toolbar;

import com.chalenger.githubconsumer.R;
import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.interceptors.SharedConstants;
import com.chalenger.githubconsumer.ui.fragments.PullRequestFragment;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by sergio on 30/06/16.
 */
public class PullRequestActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pullrequest_activity);
        ButterKnife.bind(this);
        if (savedInstanceState == null) {
            addInfoFragment();
        }
    }

    public void addInfoFragment() {
        Parcelable pullRequestParcelable = getIntent().getExtras().getParcelable(SharedConstants.EXTRA_LANGUAGE);
        LanguageGithub languageGithub = Parcels.unwrap(pullRequestParcelable);
        initToolbar(languageGithub.getNameOwner());
        PullRequestFragment pullRequestFragment = PullRequestFragment.newInstance(languageGithub);
        addFragment(R.id.fragmentContainer, pullRequestFragment);
    }

    private void initToolbar(String nameRepositoty) {
        toolbar.setTitle(nameRepositoty);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> finish());
    }
}
