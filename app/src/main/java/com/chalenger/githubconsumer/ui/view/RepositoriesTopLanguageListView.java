package com.chalenger.githubconsumer.ui.view;


import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;

import java.util.List;

/**
 * Created by sergio on 30/06/16.
 */
public interface RepositoriesTopLanguageListView {

    void showLanguages(List<LanguageGithub> list);

    void addMoore(LanguageGithub languageGithub);

    void showError(String message);

    void hideLoading();

    void showLoading();

    void showRetry();

    void hideRetry();




}
