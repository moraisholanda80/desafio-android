package com.chalenger.githubconsumer.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.chalenger.githubconsumer.R;
import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.interceptors.SharedConstants;
import com.chalenger.githubconsumer.ui.activities.PullRequestActivity;
import com.chalenger.githubconsumer.ui.adapter.RepositoriesTopLanguageAdapter;
import com.chalenger.githubconsumer.ui.adapter.OnClickListenerLanguageAdapter;
import com.chalenger.githubconsumer.ui.presenter.RepositoriesTopLanguagePresenter;
import com.chalenger.githubconsumer.ui.presenter.RepositoriesTopLanguagePresenterImpl;
import com.chalenger.githubconsumer.ui.view.RepositoriesTopLanguageListView;
import com.chalenger.githubconsumer.ui.view.MessageInfo;
import com.malinskiy.superrecyclerview.OnMoreListener;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by sergio on 309/06/16.
 */
public class RepositoriesTopGithubFragment extends BaseFragment implements RepositoriesTopLanguageListView,OnClickListenerLanguageAdapter,OnMoreListener,SwipeRefreshLayout.OnRefreshListener {

    public static final int REQUEST_CODE = 0;
    private RepositoriesTopLanguageAdapter adapter;
    private RepositoriesTopLanguagePresenter presenter;


    @BindView(R.id.recycler_view)
    com.malinskiy.superrecyclerview.SuperRecyclerView recyclerView;
    @BindView(R.id.rl_progress)
    RelativeLayout progressView;
    @BindView(R.id.rl_retry)
    RelativeLayout retryView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RepositoriesTopLanguagePresenterImpl(this);
        adapter = new RepositoriesTopLanguageAdapter();
        adapter.setListenerLanguageAdapter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.repositories_toplanguage_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
        if(savedInstanceState==null) {
            loadRepositories();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        List<LanguageGithub> languageGithubList = presenter.repositoriesParcelable();
        Parcelable parcelable = Parcels.wrap(languageGithubList);
        outState.putParcelable(SharedConstants.EXTRA_LANGUAGE_LIST,parcelable);
    }
    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            Parcelable languageParcelable = savedInstanceState.getParcelable(SharedConstants.EXTRA_LANGUAGE_LIST);
            List<LanguageGithub> languageGithubList = Parcels.unwrap(languageParcelable);
            presenter.restoreParcelable(languageGithubList);
        }
    }

    private void loadRepositories(){
        presenter.onLoad();
    }

    private void setupRecyclerView() {

        recyclerView.setAdapter(adapter);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setLoadingMore(false);
        recyclerView.setupMoreListener(this,30);
    }

    @Override
    public void showLanguages(List<LanguageGithub> list) {
        if (list != null) {
           this.adapter.add(list);
        }
    }

    @Override
    public void addMoore(LanguageGithub languageGithub) {
        this.adapter.addMoore(languageGithub);
    }

    @Override
    public void showError(String message) {
        MessageInfo.showMessage(message,R.id.container,getActivity());
    }

    @Override
    public void hideLoading() {
        recyclerView.hideMoreProgress();
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRetry() {
        retryView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
      retryView.setVisibility(View.GONE);
    }

    @Override
    public void setItemClick(LanguageGithub languageGithub) {
        Intent intent = new Intent(getActivity(),PullRequestActivity.class);
        Parcelable parcelable = Parcels.wrap(languageGithub);
        intent.putExtra(SharedConstants.EXTRA_LANGUAGE, parcelable);
        startActivity(intent);
    }

    @OnClick(R.id.button_open_settings)
    void onClickButtonSettings(){
        startActivityForResult(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS), REQUEST_CODE);
    }
    @OnClick(R.id.button_retry)
    void onClickButtonRetry(){
        presenter.onLoad();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            presenter.onLoad();
        }
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {
        presenter.onLoadMore();
    }

    @Override
    public void onRefresh() {
        Log.d(getClass().getSimpleName(),"onfrfresh");
    }
}
