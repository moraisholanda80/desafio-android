package com.chalenger.githubconsumer.ui.presenter;


import android.content.Intent;
import android.net.Uri;

import com.chalenger.githubconsumer.GithubConsumerApplication;
import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.dto.PullRequest;
import com.chalenger.githubconsumer.domain.data.error.DefaultErrorBundle;
import com.chalenger.githubconsumer.domain.data.error.ErrorBundle;
import com.chalenger.githubconsumer.domain.data.error.ErrorMessageFactory;
import com.chalenger.githubconsumer.domain.data.error.NetworkConnectionException;
import com.chalenger.githubconsumer.domain.data.error.ResultNotFoundException;
import com.chalenger.githubconsumer.domain.data.response.SimpleObserver;
import com.chalenger.githubconsumer.domain.repository.ILanguagesGithubRepository;
import com.chalenger.githubconsumer.domain.repository.LanguagesGithubRepository;
import com.chalenger.githubconsumer.ui.view.PullRequestView;
import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sergio on 30/06/16.
 */
public class PullRequestPresenterlImpl implements PullRequestPresenter {

    private PullRequestView pullRequestView;
    private ILanguagesGithubRepository repository;
    private List<PullRequest> pullRequests;

    private String owner;
    private String repositoryName;

    public PullRequestPresenterlImpl(PullRequestView pullRequestView) {
        this.pullRequestView = pullRequestView;
        this.repository = new LanguagesGithubRepository();
    }

    @Override
    public void loadPullRequest(LanguageGithub languageGithub) {
        validateConnectivity();
        loadRemote(languageGithub.getNameOwner(),languageGithub.name);
    }

    @Override
    public void onLoad() {

    }

    @Override
    public void onClickPullRequest(PullRequest pullRequest) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(pullRequest.url));
        pullRequestView.launchActivity(intent);
    }


    private void loadRemote(String owner,String repo){
        repositoryName = repo;
        this.owner = owner;
        pullRequestView.showLoading();
            repository.loadPullRequest(owner,repo)
                    .subscribe(new SimpleObserver<List<PullRequest>>(){
                        @Override
                        public void onNext(List<PullRequest> list) {
                            if(list.size()==0){
                                shorErrorMessage(new DefaultErrorBundle(new ResultNotFoundException()));
                            }else {
                                pullRequestView.hideLoading();
                                pullRequestView.showPullRequest(list);
                            }
                        }
                        @Override
                        public void onError(Throwable e) {
                            shorErrorMessage(new DefaultErrorBundle((Exception) e));
                        }

                        @Override
                        public void onCompleted() {
                           pullRequestView.hideLoading();
                            pullRequestView.hideRetry();
                        }
                    });

    }

    private void validateConnectivity(){
        new ReactiveNetwork().observeInternetConnectivity()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::notifiyConnectivity);

    }
    private void notifiyConnectivity(boolean isConnectedToInternet){
        if(!isConnectedToInternet && pullRequestView.isFragmentActive()){
            pullRequestView.hideLoading();
            pullRequestView.showRetry();
            shorErrorMessage(new DefaultErrorBundle(new NetworkConnectionException()));
        }else{
            loadRemote(owner,repositoryName);
        }

    }

    private void shorErrorMessage(ErrorBundle errorBundle){
        pullRequestView.showError(ErrorMessageFactory.create(GithubConsumerApplication.getInstance(),errorBundle.getException()));
    }
}
