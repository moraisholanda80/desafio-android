package com.chalenger.githubconsumer.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.chalenger.githubconsumer.R;
import com.chalenger.githubconsumer.domain.data.dto.PullRequest;
import com.chalenger.githubconsumer.ui.viewholder.AbstractRecyclerViewHolder;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by sergio on 01/06/16.
 */
public class PullRequestAdapter extends RecyclerView.Adapter<AbstractRecyclerViewHolder> {


    private OnClickListenerPullRequestAdapter listenerPullRequestAdapter;
    private List<PullRequest> languageGithubList = new ArrayList<>();

    public void setListenerPullRequestAdapter(OnClickListenerPullRequestAdapter listenerPullRequestAdapter) {
        this.listenerPullRequestAdapter = listenerPullRequestAdapter;
    }

    public void add(List<PullRequest> languageGithubList){
        this.languageGithubList.addAll(languageGithubList);
        notifyDataSetChanged();

    }
    public void addMoore(PullRequest languageGithub){
        this.languageGithubList.add(this.languageGithubList.size(),languageGithub) ;
        notifyItemChanged(this.languageGithubList.size());
    }
    @Override
    public AbstractRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pullrequest_item,parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AbstractRecyclerViewHolder holder, int position) {
        ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        PullRequest pullRequest = languageGithubList.get(position);
        itemViewHolder.textViewTitlePullRequest.setText(pullRequest.title);
        itemViewHolder.textViewAuthor.setText(pullRequest.getNameUser());
        itemViewHolder.textViewBodyPullRequest.setText(pullRequest.body);
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(itemViewHolder.view.getContext());
        itemViewHolder.textViewDate.setText(dateFormat.format(pullRequest.createdAt));
        Glide.with(itemViewHolder.view.getContext())
                .load(pullRequest.getUrlAvatar())
                .bitmapTransform(new CropCircleTransformation(itemViewHolder.view.getContext()))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(itemViewHolder.imageView);
        itemViewHolder.itemRow.setOnClickListener(v -> {
            if(listenerPullRequestAdapter!=null){
                listenerPullRequestAdapter.setItemClick(pullRequest);
            }
        });

    }


    @Override
    public int getItemCount() {
        return languageGithubList.size();
    }


    class ItemViewHolder extends AbstractRecyclerViewHolder {
        View view;
        @BindView(R.id.author_avatar)
        ImageView imageView;
        @BindView(R.id.title_pullrequest)
        TextView textViewTitlePullRequest;
        @BindView(R.id.item_row)
        LinearLayout itemRow;
        @BindView(R.id.body_pullrequest)
        TextView textViewBodyPullRequest;
        @BindView(R.id.username)
        TextView textViewAuthor;
        @BindView(R.id.date_pullrequest)
        TextView textViewDate;

        public ItemViewHolder(View itemView) {
            super(itemView);
            view = itemView;

        }
    }


}
