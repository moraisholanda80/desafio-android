package com.chalenger.githubconsumer.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.chalenger.githubconsumer.R;
import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.dto.PullRequest;
import com.chalenger.githubconsumer.domain.data.interceptors.SharedConstants;
import com.chalenger.githubconsumer.ui.adapter.OnClickListenerPullRequestAdapter;
import com.chalenger.githubconsumer.ui.adapter.PullRequestAdapter;
import com.chalenger.githubconsumer.ui.presenter.PullRequestPresenter;
import com.chalenger.githubconsumer.ui.presenter.PullRequestPresenterlImpl;
import com.chalenger.githubconsumer.ui.view.MessageInfo;
import com.chalenger.githubconsumer.ui.view.PullRequestView;
import com.malinskiy.superrecyclerview.OnMoreListener;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by sergio on 30/06/16.
 */
public class PullRequestFragment extends BaseFragment implements PullRequestView,OnClickListenerPullRequestAdapter,OnMoreListener {


    @BindView(R.id.recycler_view)
    com.malinskiy.superrecyclerview.SuperRecyclerView recyclerView;
    @BindView(R.id.rl_progress)
    RelativeLayout progressView;
    @BindView(R.id.rl_retry)
    RelativeLayout retryView;

    private LanguageGithub languageGithub;
    private PullRequestPresenter presenter = new PullRequestPresenterlImpl(this);

    private PullRequestAdapter adapter;
    public static final int REQUEST_CODE = 0;

    public static PullRequestFragment newInstance(LanguageGithub languageGithub){
        Bundle args = new Bundle();
        Parcelable parcelable = Parcels.wrap(languageGithub);
        args.putParcelable(SharedConstants.EXTRA_LANGUAGE, parcelable);

        PullRequestFragment characterInfoFragment = new PullRequestFragment();
        characterInfoFragment.setArguments(args);
        return characterInfoFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       adapter = new PullRequestAdapter();
        adapter.setListenerPullRequestAdapter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.pullrequest_fragment, container,false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupRecyclerView();
        if (getArguments() != null) {
            languageGithub = getParcelableFromArgs();
            presenter.loadPullRequest(languageGithub);
        }
    }

    private LanguageGithub getParcelableFromArgs() {
        Parcelable parcelable = getArguments().getParcelable(SharedConstants.EXTRA_LANGUAGE);
        return Parcels.unwrap(parcelable);
    }
    @Override
    public void showPullRequest(List<PullRequest> pullRequestList) {
        if (pullRequestList != null) {
            this.adapter.add(pullRequestList);
        }
    }
    private void setupRecyclerView() {
        recyclerView.setAdapter(adapter);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setLoadingMore(false);
    }
    @OnClick(R.id.button_open_settings)
    void onClickButtonSettings(){
        startActivityForResult(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS), REQUEST_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            presenter.onLoad();
        }
    }
    @Override
    public void showError(String message) {
        MessageInfo.showMessage(message,R.id.container,getActivity());
    }

    @Override
    public void hideLoading() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void showRetry() {
        retryView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRetry() {
     retryView.setVisibility(View.GONE);
    }

    @Override
    public void launchActivity(Intent intent) {
        startActivity(intent);
    }

    @Override
    public boolean isFragmentActive() {
        return isFragmentUIActive();
    }

    @Override
    public void setItemClick(PullRequest pullRequest) {
        presenter.onClickPullRequest(pullRequest);
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {

    }
    public boolean isFragmentUIActive() {
        return isAdded() && !isDetached() && !isRemoving();
    }
}
