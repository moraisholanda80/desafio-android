package com.chalenger.githubconsumer;

import android.app.Application;

/**
 * Created by sergio on 30/06/16.
 */
public class GithubConsumerApplication extends Application {
    private static GithubConsumerApplication instance =null;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static GithubConsumerApplication getInstance() {
        return instance;
    }
}
