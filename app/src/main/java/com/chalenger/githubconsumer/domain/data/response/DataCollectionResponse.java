package com.chalenger.githubconsumer.domain.data.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sergio on 30/06/16.
 */
public class DataCollectionResponse <T> {

    @SerializedName("total_count")
    public int totalCount;

    @SerializedName("incomplete_results")
    public boolean incompleteResults;

    public T items  ;


}
