package com.chalenger.githubconsumer.domain.data.interceptors;

/**
 * Created by sergio on 30/06/16.
 */
public class SharedConstants {;
    public static final String EXTRA_LANGUAGE_LIST = "extraLangUageList";
    public static final String EXTRA_LANGUAGE = "extraLanguage";
}
