package com.chalenger.githubconsumer.domain.tools;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.chalenger.githubconsumer.GithubConsumerApplication;


/**
 * Created by sergio on 21/06/16.
 */
public class ConectivityUtil {

    public static boolean isThereInternetConnection() {
        boolean isConnected;


        ConnectivityManager connectivityManager =
                (ConnectivityManager) GithubConsumerApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());

        return isConnected;
    }
}
