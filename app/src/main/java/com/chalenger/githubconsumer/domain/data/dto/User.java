package com.chalenger.githubconsumer.domain.data.dto;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by sergio on 03/07/16.
 */

@Parcel
public class User {

    public String login;

    @SerializedName("avatar_url")
    public String avatarUrl;
}
