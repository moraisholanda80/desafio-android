package com.chalenger.githubconsumer.domain.repository;


import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.dto.PullRequest;

import java.util.List;

import rx.Observable;

/**
 * Created by sergio on 30/06/16.
 */
public interface ILanguagesGithubRepository {

    Observable<List<LanguageGithub>> loadRespositories(String language,String sort,int page);

    Observable<List<PullRequest>> loadPullRequest(String repo, String owner);
}
