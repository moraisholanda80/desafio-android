package com.chalenger.githubconsumer.domain.data.api;


import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.dto.PullRequest;
import com.chalenger.githubconsumer.domain.data.response.DataCollectionResponse;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by sergio on 01/07/16.
 */
public interface LanguageGithubApi {

    @GET("/search/repositories")
    Observable<DataCollectionResponse<List<LanguageGithub>>> getRepositoriesLanguages(@Query("q")String language, @Query("sort")String sort, @Query("page")String page);

    @GET("/repos/{creator}/{repo}/pulls")
    Observable<List<PullRequest>> getPullRequests(@Path("creator") String creator, @Path("repo") String repo);
}
