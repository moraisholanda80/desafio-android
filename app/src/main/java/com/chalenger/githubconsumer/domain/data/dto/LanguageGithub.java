package com.chalenger.githubconsumer.domain.data.dto;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by sergio on 01/07/16.
 */

@Parcel
public class LanguageGithub {

    public int id;

    @SerializedName("total_count")
    public int totalCount;

    public String name;

    public boolean fork;

    @SerializedName("stargazers_count")
    public int stars;

    @SerializedName("forks_count")
    public int countForks;

    public String description;

    public String nameOwner;

    public OwnerGithub owner;

    public String getNameOwner() {
        return owner.login;
    }

    public String getUrlAvatar(){
        return  owner.avatarUser;
    }

}
