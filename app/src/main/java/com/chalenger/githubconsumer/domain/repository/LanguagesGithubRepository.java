package com.chalenger.githubconsumer.domain.repository;


import com.chalenger.githubconsumer.domain.data.api.ApiFactory;
import com.chalenger.githubconsumer.domain.data.dto.LanguageGithub;
import com.chalenger.githubconsumer.domain.data.dto.PullRequest;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sergio on 30/06/16.
 */
public class LanguagesGithubRepository implements ILanguagesGithubRepository {

    @Override
    public Observable<List<LanguageGithub>> loadRespositories(String language, String sort, int page) {
        return ApiFactory.repositoriesApi().getRepositoriesLanguages(language,sort,String.valueOf(page))
                .subscribeOn(Schedulers.io())
                .map(listDataCollectionResponse -> listDataCollectionResponse.items)
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<PullRequest>> loadPullRequest(String repo, String owner) {
        return ApiFactory.repositoriesApi().getPullRequests(repo, owner)
                .subscribeOn(Schedulers.io())
                .map(pullRequestDataCollectionResponse -> pullRequestDataCollectionResponse)
                .observeOn(AndroidSchedulers.mainThread());
    }
}
