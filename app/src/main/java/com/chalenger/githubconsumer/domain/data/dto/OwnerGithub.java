package com.chalenger.githubconsumer.domain.data.dto;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

/**
 * Created by sergio on 30/06/16.
 */
@Parcel
public class OwnerGithub {

    @SerializedName("avatar_url")
    public String avatarUser;

    public String login;

}
