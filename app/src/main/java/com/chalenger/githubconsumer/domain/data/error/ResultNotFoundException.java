package com.chalenger.githubconsumer.domain.data.error;

/**
 * Created by sergio on 01/07/16.
 */
public class ResultNotFoundException extends Exception {

    public ResultNotFoundException() {
        super();
    }

    public ResultNotFoundException(final String message) {
        super(message);
    }

    public ResultNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ResultNotFoundException(final Throwable cause) {
        super(cause);
    }

}
