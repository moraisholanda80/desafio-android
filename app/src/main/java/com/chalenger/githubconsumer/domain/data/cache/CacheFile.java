package com.chalenger.githubconsumer.domain.data.cache;

import com.chalenger.githubconsumer.GithubConsumerApplication;
import com.chalenger.githubconsumer.domain.data.error.DefaultErrorBundle;

import java.io.File;

import okhttp3.Cache;
import timber.log.Timber;

/**
 * Created by sergio on 03/07/16.
 */
public class CacheFile {

    public static final String HTTP_CACHE = "http-cache";

    public static Cache provideCache() {
        Cache cache = null;
        try {
            cache = new Cache(new File(GithubConsumerApplication.getInstance().getCacheDir(), HTTP_CACHE),
                    10 * 1024 * 1024); // 10 MB
        } catch (Exception e) {
            Timber.e(e, new DefaultErrorBundle(e).getErrorMessage());
        }
        return cache;
    }
}
