package com.chalenger.githubconsumer.domain.data.dto;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.Date;

/**
 * Created by sergio on 03/07/16.
 */
@Parcel
public class PullRequest {

    @SerializedName("html_url")
    public String url;

    public String state;

    public String title;

    public String body;

    public User user;

    public String nameUser;

    public String urlAvatar;;

    public String getUrlAvatar() {
        return user.avatarUrl;
    }

    public String getNameUser() {
        return user.login;
    }

    @SerializedName("created_at")
    public Date createdAt;

}
