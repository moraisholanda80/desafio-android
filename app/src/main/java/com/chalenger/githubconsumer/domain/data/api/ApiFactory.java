package com.chalenger.githubconsumer.domain.data.api;

import com.chalenger.githubconsumer.BuildConfig;
import com.chalenger.githubconsumer.domain.data.interceptors.LogginInterceptor;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.chalenger.githubconsumer.domain.data.cache.CacheFile.provideCache;
import static com.chalenger.githubconsumer.domain.data.interceptors.NetworkInterceptor.provideCacheInterceptor;
import static com.chalenger.githubconsumer.domain.data.interceptors.NetworkOfflineInterceptor.provideOfflineCacheInterceptor;

/**
 * Created by sergio on 01/07/16.
 */
public class ApiFactory {

    public static Retrofit provideRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL)
                .client(provideOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private static OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(LogginInterceptor.provideHttpLoggingInterceptor())
                .addInterceptor(provideOfflineCacheInterceptor())
                .addNetworkInterceptor(provideCacheInterceptor())
                .cache(provideCache())
                .build();
    }

    public static LanguageGithubApi repositoriesApi() {
        return provideRetrofit().create(LanguageGithubApi.class);
    }
}
