package com.chalenger.githubconsumer.domain.data.error;

/**
 * Created by sergio on 01/07/16.
 */
public interface ErrorBundle {
    Exception getException();

    String getErrorMessage();
}
