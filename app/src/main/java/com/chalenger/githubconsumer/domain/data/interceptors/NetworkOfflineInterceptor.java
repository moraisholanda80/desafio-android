package com.chalenger.githubconsumer.domain.data.interceptors;

import com.chalenger.githubconsumer.domain.tools.ConectivityUtil;

import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;

/**
 * Created by sergio on 03/07/16.
 */
public class NetworkOfflineInterceptor {
    private boolean isConnectedToInternet;

    public static Interceptor provideOfflineCacheInterceptor() {
        return chain -> {
            Request request = chain.request();
            if (!ConectivityUtil.isThereInternetConnection()) {
                CacheControl cacheControl = new CacheControl.Builder()
                        .maxStale(7, TimeUnit.DAYS)
                        .build();

                request = request.newBuilder()
                        .cacheControl(cacheControl)
                        .build();
            }

            return chain.proceed(request);
        };
    }
}
